## Install docker, docker compose

```bash
sudo apt-get update
sudo apt install git docker docker-compose
sudo service docker start && sudo systemctl enable docker

```
## git clone relo

```bash
git clone https://gitlab.com/nyinyisoepaing/restreamer.git
```

## run project
```bash
cd restreamer 
docker-compose up -d
```
